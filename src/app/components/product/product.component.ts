import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/product.model';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  products: Product [] = [];

  product: Product = {
    id:'',
    title:'',
    image:[],
    price:0,
    description:'',
    category:''
  };

  counter = 0;

  constructor(
    private ProductService: ProductsService
  ) { }

  ngOnInit(): void {
    this.ProductService.getAllProducts()
    .subscribe(data => {
      this.products = data;
      console.log(this.products);
      this.product = this.products[0];
    })
    console.log(this.counter);
  }

  showNextProduct(){
    console.log(this.counter);
    if(this.counter > this.products.length || this.counter < 0){
      this.counter = 0;
      return this.products[0];
    }else{
      this.counter = this.counter + 1;
      this.product = this.products[this.counter];
      console.log(this.product);
    }
    return this.product;
  }
  showPrevProduct(){
    console.log(this.counter);
    if(this.counter > this.products.length || this.counter < 0){
      this.counter = 0;
      return this.products[0];
    }else{
      this.counter = this.counter -1;
      this.product = this.products[this.counter];
      console.log(this.product);
    }
    return this.product;
  }
}
