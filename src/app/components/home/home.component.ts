import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  images: Array<any> = [
    { name: '../../../assets/20200918_150058_2.jpg' },
    { name: '../../../assets/20211027_223652.jpg' },
    { name: '../../../assets/IMG-20180710-WA0000.jpg' },
  ];

  constructor() { }

  ngOnInit(): void {}


}
