import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  navImg: string = '../../../assets/20200918_150058_2.jpg';
  showMenu = false;

  constructor() { }

  ngOnInit(): void {
  }
  toggleMenu(){
    this.showMenu = !this.showMenu;
    console.log(this.showMenu);
  }
}
